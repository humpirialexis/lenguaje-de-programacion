Algoritmo CalcularNominaObreros
    Definir horas_trabajadas, salario_hora, salario_total Como Real
    Definir contador Como Entero
    salario_hora <- 30000
    Para contador <- 1 Hasta 50 Con Paso 1 Hacer
        Escribir "Ingrese las horas trabajadas del obrero ", contador, ":"
        Leer horas_trabajadas
        salario_total <- horas_trabajadas * salario_hora
        Escribir "El obrero ", contador, " debe recibir ", salario_total, " Bolívares por sus horas trabajadas."
    FinPara
FinAlgoritmo


Algoritmo CalcularNominaObreros
    Definir horas_trabajadas, salario_hora, salario_total Como Real
    Definir contador Como Entero
    salario_hora <- 30000
    contador <- 1
    
    Repetir
        Escribir "Ingrese las horas trabajadas del obrero ", contador, ":"
        Leer horas_trabajadas
        salario_total <- horas_trabajadas * salario_hora
        Escribir "El obrero ", contador, " debe recibir ", salario_total, " Bolívares por sus horas trabajadas."
        contador <- contador + 1
    Hasta Que contador > 50
FinAlgoritmo



//¿Qué pasaría si no se decrementa al número de obreros en uno?
//Si no decrementas el contador al número de obreros en uno dentro
//que ocurra un error de desbordamiento de memoria, dependiendo del
//lenguaje de programación que estés utilizando.

//En el ejemplo proporcionado, el bucle "Para" está diseñado para
//iterar desde 1 hasta 50, incrementando el contador en 1 en cada 
//iteración. Si no decrementas el contador al número de obreros en 
//uno (es decir, no utilizas "contador <- contador + 1" o "contador++" 
//dentro del bucle), el contador nunca alcanzará el valor 50 y el bucle 
//continuará ejecutándose repetidamente, lo que se conoce como un bucle 
//infinito.

//Para evitar un bucle infinito, es importante asegurarse de que el 
//contador se incremente o decremente apropiadamente dentro del bucle
//para que el bucle tenga una condición de terminación definida. 
//En este caso, decrementar el contador dentro del bucle es esencial 
//para que el bucle termine después de que se procesen todas las 50 
//entradas de los obreros.







//
//
//
//
