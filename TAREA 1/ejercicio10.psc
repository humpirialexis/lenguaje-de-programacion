Proceso CalcularPagoEntradas
    // Declaración de variables
    Definir cantidadEntradas, costoTotal, descuento, pagoFinal Como Real
    
    // Solicitar al usuario que ingrese la cantidad de entradas
    Escribir "Ingrese la cantidad de entradas a comprar (1-4):"
    Leer cantidadEntradas
    
    // Verificar la cantidad de entradas y aplicar descuento correspondiente
    Según cantidadEntradas Hacer
        Caso 1:
            costoTotal = 1 * 100  // El costo de una entrada es de 100 unidades monetarias
        Caso 2:
            costoTotal = 2 * 100  // El costo de dos entradas es de 200 unidades monetarias
            descuento = costoTotal * 0.10  // Aplicar descuento del 10%
            costoTotal = costoTotal - descuento
        Caso 3:
            costoTotal = 3 * 100  // El costo de tres entradas es de 300 unidades monetarias
            descuento = costoTotal * 0.15  // Aplicar descuento del 15%
            costoTotal = costoTotal - descuento
        Caso 4:
            costoTotal = 4 * 100  // El costo de cuatro entradas es de 400 unidades monetarias
            descuento = costoTotal * 0.20  // Aplicar descuento del 20%
            costoTotal = costoTotal - descuento
        De Otro Modo:
            Escribir "Cantidad de entradas no válida."
    Fin Según
    
    // Mostrar el pago final
    Escribir "El pago a realizar es:", costoTotal, "unidades monetarias."
FinProceso
