Algoritmo ParImpar
    // Declaración de variables
    Definir N Como Entero
    
    // Solicitar al usuario que ingrese un número
    Escribir "Ingrese un número:"
    Leer N
    
    // Verificar si el número es par o impar
    Si N Mod 2 = 0 Entonces
        Escribir "El número", N, "es par."
    Sino
        Escribir "El número", N, "es impar."
    FinSi
FinAlgoritmo
