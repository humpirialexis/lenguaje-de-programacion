Proceso CalcularMediaAritmetica
    // Declaración de variables
    Definir cantidadNumeros, suma, media Como Real
    Definir numero Como Real // Variable para almacenar cada número ingresado
    
    // Inicializar la suma
    suma = 0
    
    // Solicitar al usuario la cantidad de números
    Escribir "Ingrese la cantidad de números:"
    Leer cantidadNumeros
    
    // Leer y sumar los números
    Para i desde 1 hasta cantidadNumeros hacer
        Escribir "Ingrese el número ", i, ":"
        Leer numero
        suma = suma + numero
    Fin Para
    
    // Calcular la media aritmética
    media = suma / cantidadNumeros
    
    // Mostrar el resultado
    Escribir "La media aritmética es:", media
FinProceso
