Proceso Escribir_100_Naturales
    // Inicializar el contador
    Definir contador como Entero
    contador = 1
	
    // Mientras el contador sea menor o igual a 100, escribir el número natural y aumentar el contador
    Mientras (contador <= 100) Hacer
        Escribir contador
        contador = contador + 1
    Fin Mientras
Fin Proceso
