Algoritmo mayor_de_dos_valores
    // Inicio
    // Declaración de variables: A = 0, B = 0
    Escribir "Introduzca dos valores distintos:"
    Leer A, B

    // Si A = B Entonces
    Si A = B Entonces
        // Volver al paso 3 porque los valores deben ser distintos
        Escribir "Los valores deben ser distintos, intente nuevamente."
        Leer A, B
    FinSi

    // Si A > B Entonces
    Si A > B Entonces
        // Escribir A, "Es el mayor"
        Escribir A, "Es el mayor"
    Sino
        // De lo contrario: Escribir B, "Es el mayor"
        Escribir B, "Es el mayor"
    FinSi

    // Fin
FinAlgoritmo