Proceso Convertir_Calificacion
    // Inicializar la calificación como un valor fuera del rango válido
    calificacion = 0
    
    // Mientras la calificación esté fuera del rango válido, solicitar al usuario que ingrese la calificación
    Mientras calificacion < 1 o calificacion > 20 Hacer
        // Solicitar al usuario que ingrese la calificación numérica
        Escribir "Ingrese la calificación numérica:"
        Leer calificacion
        
        // Verificar si la calificación está dentro del rango válido (1-20)
        Si calificacion < 1 o calificacion > 20 Entonces
            Escribir "La calificación ingresada no es válida."
        Fin Si
    Fin Mientras
    
    // Determinar la letra correspondiente a la calificación
    Si calificacion >= 19 Entonces
        letra = "A"
    Sino 
        Si calificacion >= 16 Entonces
            letra = "B"
        Sino 
            Si calificacion >= 13 Entonces
                letra = "C"
            Sino 
                Si calificacion >= 10 Entonces
                    letra = "D"
                Sino
                    letra = "E"
                Fin Si
            Fin Si
        Fin Si
    Fin Si
    
    // Mostrar la calificación en letra
    Escribir "La calificación en letra es:", letra
Fin Proceso




