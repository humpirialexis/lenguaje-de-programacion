Proceso ConvertirVelocidad
    // Declaración de variables
    Definir velocidad_ms, velocidad_kmh Como Real
    
    // Solicitar al usuario que ingrese la velocidad en metros por segundo
    Escribir "Ingrese la velocidad en metros por segundo:"
    Leer velocidad_ms
    
    // Convertir la velocidad a kilómetros por hora
    velocidad_kmh = velocidad_ms * 3.6
    
    // Mostrar el resultado
    Escribir "La velocidad es:", velocidad_kmh, "kilómetros por hora."
FinProceso
