Algoritmo ConversionDias
    Definir dias, años, meses, semanas, dias_restantes Como Entero

    Escribir "Ingrese el número de días:"
    Leer dias

    años <- 0
    meses <- 0
    semanas <- 0

    Repetir
        Si dias >= 365 Entonces
            años <- años + 1
            dias <- dias - 365
        Sino
            Si dias >= 30 Entonces
                meses <- meses + 1
                dias <- dias - 30
            Sino
                Si dias >= 7 Entonces
                    semanas <- semanas + 1
                    dias <- dias - 7
                Sino
                    dias_restantes <- dias
                    dias <- 0
                FinSi
            FinSi
        FinSi
    Hasta Que dias = 0

    Escribir "Años:", años
    Escribir "Meses:", meses
    Escribir "Semanas:", semanas
    Escribir "Días:", dias_restantes

FinAlgoritmo




Algoritmo ConversionDias
    Definir dias, años, meses, semanas, dias_restantes Como Entero
	
    Escribir "Ingrese el número de días:"
    Leer dias
	
    años <- 0
    meses <- 0
    semanas <- 0
	
    Si dias >= 365 Entonces
        años <- dias / 365
        dias <- dias MOD 365
    FinSi
	
    Si dias >= 30 Entonces
        meses <- dias / 30
        dias <- dias MOD 30
    FinSi
	
    Si dias >= 7 Entonces
        semanas <- dias / 7
        dias <- dias MOD 7
    FinSi
	
    dias_restantes <- dias
	
    Escribir "Años:", años
    Escribir "Meses:", meses
    Escribir "Semanas:", semanas
    Escribir "Días:", dias_restantes
	
FinAlgoritmo
