Algoritmo Sumatoria
    Definir N, Suma Como Entero
	
    N <- 0
    Suma <- 0
	
    Repetir
        N <- N + 1
        Suma <- Suma + N
        Si N = 10 Entonces
            Escribir "La sumatoria de los números enteros entre 1 y 10 es: ", Suma
        FinSi
    Hasta Que N = 10
FinAlgoritmo
