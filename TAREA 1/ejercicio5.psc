Algoritmo AreaRectangulo
    // Declaración de variables
    Definir base, altura, area Como Real
    
    // Solicitar al usuario que ingrese la base del rectángulo
    Escribir "Ingrese la base del rectángulo:"
    Leer base
    
    // Solicitar al usuario que ingrese la altura del rectángulo
    Escribir "Ingrese la altura del rectángulo:"
    Leer altura
    
    // Calcular el área del rectángulo
    area <- base * altura
    
    // Mostrar el resultado al usuario
    Escribir "El área del rectángulo es:", area
FinAlgoritmo
