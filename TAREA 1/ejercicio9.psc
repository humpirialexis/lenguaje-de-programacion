Proceso Multiplos_de_2
    // Inicializar el contador y el número de múltiplos encontrados
    Definir contador como Entero
    Definir multiplos_encontrados como Entero
    contador = 1
    multiplos_encontrados = 0

    // Mientras no se hayan encontrado 20 múltiplos de 2
    Mientras multiplos_encontrados < 20 Hacer
        // Verificar si el número actual es múltiplo de 2
        Si contador % 2 == 0 Entonces
            Escribir contador, " es múltiplo de 2."
            multiplos_encontrados = multiplos_encontrados + 1
        Fin Si

        // Incrementar el contador
        contador = contador + 1
    Fin Mientras
Fin Proceso


Proceso CalcularMinutosEn5Horas
    // Calcular cuántos minutos hay en 5 horas
    Definir horas, minutosTotales Como Entero
    
    // Asignar el valor de 5 a la variable horas
    horas = 5
    
    // Calcular los minutos totales en 5 horas
    minutosTotales = horas * 60
    
    // Mostrar el resultado
    Escribir "En", horas, "horas hay", minutosTotales, "minutos."
FinProceso



//¿Qué falta en este algoritmo?
//Falta de manejo de la variable S: La variable S se utiliza para
//contar la cantidad de divisores encontrados, sin embargo, no se
//reinicia en cada iteración del ciclo. Esto puede llevar a
//resultados incorrectos si el mismo algoritmo se ejecuta
//varias veces sin reiniciar S.
//
//Falta de manejo de los casos especiales: El algoritmo no maneja
//correctamente los casos especiales donde N es igual a 0 o 1. 
//Tampoco considera el caso de números negativos.
//
//
//¿ Qué errores presenta?
//Error en la condición de divisibilidad: La condición N / J = 0 no es correcta
 //para verificar la divisibilidad. Debería ser N MOD J = 0.
//
//
//Mal uso de la variable J: La variable J debería empezar desde
//2 y avanzar hasta N - 1 (o N / 2 para optimización), pero el
//algoritmo actualmente avanza J solo una vez por iteración, lo
//que significa que no verifica todos los posibles divisores.

