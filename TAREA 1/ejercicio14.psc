Algoritmo CajaRegistradora
    Definir total_compra, pago_cliente, cambio Como Real
	
    Escribir "Bienvenido a la caja registradora"
    Escribir "Por favor, ingrese el total de la compra:"
    Leer total_compra
	
    Escribir "Por favor, ingrese la cantidad pagada por el cliente:"
    Leer pago_cliente
	
    Si pago_cliente >= total_compra Entonces
        cambio <- pago_cliente - total_compra
        Escribir "El cambio es: ", cambio
    Sino
        Escribir "El cliente no ha pagado lo suficiente. Faltan ", total_compra - pago_cliente
    FinSi
	
FinAlgoritmo
