Algoritmo mayor_y_menor_de_tres_valores
    // Inicio
    // Declaración de variables: A = 0, B = 0, C = 0
    Escribir "Introduzca tres valores distintos:"
    Leer A, B, C

    // Si A = B o A = C o B = C Entonces
    Si A = B o A = C o B = C Entonces
        // Volver al paso 3 porque los valores deben ser distintos
        Escribir "Los valores deben ser distintos, intente nuevamente."
        Leer A, B, C
    FinSi

    // Si A > B y A > C Entonces
    Si A > B y A > C Entonces
        // Escribir A, "Es el mayor"
        Escribir A, "Es el mayor"
        // Si B > C Entonces
        Si B > C Entonces
            // Escribir B, "Es el menor"
            Escribir B, "Es el menor"
        Sino
            // De lo contrario: Escribir C, "Es el menor"
            Escribir C, "Es el menor"
        FinSi
    Sino
        // Si B > A y B > C Entonces
        Si B > A y B > C Entonces
            // Escribir B, "Es el mayor"
            Escribir B, "Es el mayor"
            // Si A > C Entonces
            Si A > C Entonces
                // Escribir A, "Es el menor"
                Escribir A, "Es el menor"
            Sino
                // De lo contrario: Escribir C, "Es el menor"
                Escribir C, "Es el menor"
            FinSi
        Sino
            // De lo contrario: Escribir C, "Es el mayor"
            Escribir C, "Es el mayor"
            // Si A > B Entonces
            Si A > B Entonces
                // Escribir A, "Es el menor"
                Escribir A, "Es el menor"
            Sino
                // De lo contrario: Escribir B, "Es el menor"
                Escribir B, "Es el menor"
            FinSi
        FinSi
    FinSi

    // Fin
FinAlgoritmo