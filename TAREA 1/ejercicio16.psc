Algoritmo ContarBilletes
    Definir BC, BV, BD, BC2, BM, Resto, N, C Como Entero

    Escribir "Ingrese el monto total en Bolívares:"
    Leer N

    C <- N

    Si C >= 50000 Entonces
        BC <- BC + 1
        C <- C - 50000
    FinSi

    Si C >= 20000 Entonces
        BV <- BV + 1
        C <- C - 20000
    FinSi

    Si C >= 10000 Entonces
        BD <- BD + 1
        C <- C - 10000
    FinSi

    Si C >= 5000 Entonces
        BC2 <- BC2 + 1
        C <- C - 5000
    FinSi

    Si C >= 1000 Entonces
        BM <- BM + 1
        C <- C - 1000
    FinSi

    Resto <- C

    Escribir "Cantidad de billetes de 50.000 Bolívares:", BC
    Escribir "Cantidad de billetes de 20.000 Bolívares:", BV
    Escribir "Cantidad de billetes de 10.000 Bolívares:", BD
    Escribir "Cantidad de billetes de 5.000 Bolívares:", BC2
    Escribir "Cantidad de billetes de 1.000 Bolívares:", BM
    Escribir "Resto:", Resto

FinAlgoritmo
