Algoritmo DeterminarLetraCalificacion
    Definir calificacion Como Entero;
	
    Escribir "Ingrese la calificación (0-10):";
    Leer calificacion;
	
    Si calificacion == 10 Entonces
        Escribir "La calificación correspondiente es: A";
    Sino
        Si calificacion == 9 Entonces
            Escribir "La calificación correspondiente es: B";
        Sino
            Si calificacion == 8 Entonces
                Escribir "La calificación correspondiente es: C";
            Sino
                Si calificacion >= 6 Entonces
                    Escribir "La calificación correspondiente es: D";
                Sino
                    Escribir "La calificación correspondiente es: F";
                FinSi
            FinSi
        FinSi
    FinSi
FinAlgoritmo
