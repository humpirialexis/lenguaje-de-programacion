Algoritmo CalcularPrecioVenta
    Definir modelo, talla, cantidad Como Entero;
    Definir precio_tela_A, precio_tela_B, mano_de_obra_A, mano_de_obra_B Como Real;
    Definir costo_tela, costo_mano_de_obra, precio_final, ganancia_total Como Real;
	
    precio_tela_A <- 1.50;  // Precio de la tela para el modelo A
    precio_tela_B <- 1.80;  // Precio de la tela para el modelo B
    mano_de_obra_A <- 0.80;  // Porcentaje del costo de la tela para la mano de obra del modelo A
    mano_de_obra_B <- 0.95;  // Porcentaje del costo de la tela para la mano de obra del modelo B
	
    Escribir "Ingrese el modelo de pantalón (1 para A, 2 para B):";
    Leer modelo;
	
    Escribir "Ingrese la talla del pantalón (30, 32 o 36):";
    Leer talla;
	
    Escribir "Ingrese la cantidad de pantalones a producir:";
    Leer cantidad;
	
    // Calcular costo de la tela y la mano de obra según el modelo
    Si modelo == 1 Entonces
        costo_tela <- cantidad * precio_tela_A * 1.50;
        costo_mano_de_obra <- cantidad * precio_tela_A * mano_de_obra_A;
    Sino
        Si modelo == 2 Entonces
            costo_tela <- cantidad * precio_tela_B * 1.80;
            costo_mano_de_obra <- cantidad * precio_tela_B * mano_de_obra_B;
        FinSi;
    FinSi;
	
    // Calcular precio final y ganancia total
    precio_final <- costo_tela + costo_mano_de_obra;
    ganancia_total <- precio_final * 0.20;  // 20% de ganancia
	
    Escribir "El precio final de venta es:", precio_final;
    Escribir "La ganancia total es:", ganancia_total;
FinAlgoritmo
