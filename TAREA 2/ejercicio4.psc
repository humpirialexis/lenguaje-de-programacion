Algoritmo CalcularCostoLlamada
    Definir duracion_llamada, costo_por_minuto, costo_total Como Real;
	
    Escribir "Ingrese la duración de la llamada en minutos:";
    Leer duracion_llamada;
	
    Escribir "Ingrese el costo por minuto de la llamada:";
    Leer costo_por_minuto;
	
    costo_total <- duracion_llamada * costo_por_minuto;
	
    Escribir "El costo total de la llamada es:", costo_total;
FinAlgoritmo
