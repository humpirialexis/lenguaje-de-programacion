Algoritmo CalcularEdad
    Definir año_actual, año_nacimiento, edad Como Entero;
    
    Escribir "Ingrese el año actual:";
    Leer año_actual;
    
    Escribir "Ingrese el año de nacimiento:";
    Leer año_nacimiento;
    
    edad <- año_actual - año_nacimiento;
    
    Escribir "La edad de la persona es:", edad, "años";
FinAlgoritmo
