Algoritmo CalcularCostoPintura
    Definir m2, precio_por_m2, costo_total Como Real;
	
    Escribir "Ingrese la cantidad de metros cuadrados a pintar:";
    Leer m2;
	
    Escribir "Ingrese el precio por metro cuadrado:";
    Leer precio_por_m2;
	
    costo_total <- m2 * precio_por_m2;
	
    Escribir "El costo total del trabajo de pintura es:", costo_total;
FinAlgoritmo
