Algoritmo CalcularBono
    Definir antiguedad, bono Como Entero;
	
    Escribir "Ingrese la antigüedad del empleado en años:";
    Leer antiguedad;
	
    Si antiguedad == 1 Entonces
        bono <- 100;
    Sino
        Si antiguedad == 2 Entonces
            bono <- 200;
        Sino
            Si antiguedad == 3 Entonces
                bono <- 300;
            Sino
                Si antiguedad == 4 Entonces
                    bono <- 400;
                Sino
                    Si antiguedad == 5 Entonces
                        bono <- 500;
                    Sino
                        Si antiguedad > 5 Entonces
                            bono <- 1000;
                        Sino
                            Escribir "La antigüedad ingresada no es válida.";
                        FinSi
                    FinSi
                FinSi
            FinSi
        FinSi
    FinSi
	
    Escribir "El bono correspondiente es:", bono;
FinAlgoritmo
