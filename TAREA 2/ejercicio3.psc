Algoritmo CalcularTiempoViaje
    Definir distancia, velocidad, tiempo Como Real;
	
    Escribir "Ingrese la distancia entre las ciudades en kilómetros:";
    Leer distancia;
	
    Escribir "Ingrese la velocidad promedio en kilómetros por hora:";
    Leer velocidad;
	
    tiempo <- distancia / velocidad;
	
    Escribir "El tiempo estimado de viaje es:", tiempo, "horas";
FinAlgoritmo
